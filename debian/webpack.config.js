'use strict';

var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

var config = {

  target: 'web',
  resolve: {
    modules: ['/usr/share/nodejs', '.'],
  },
  resolveLoader: {
    modules: ['/usr/share/nodejs'],
  },
  node: {
    fs: 'empty'
  },
  output: {
    libraryTarget: 'umd'
  }
}

module.exports = config;

